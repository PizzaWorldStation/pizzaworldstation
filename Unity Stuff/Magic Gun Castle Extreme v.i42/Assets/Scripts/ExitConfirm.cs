﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ExitConfirm : MonoBehaviour {

	public Button exit;
	public Button confirm;
	public Button cancel;
	public GameObject g;

	// Use this for initialization
	void start ()
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		exit.onClick.AddListener(()=> {
			g.SetActive(true);
		});

		cancel.onClick.AddListener(()=> {
			g.SetActive(false);
		});

		confirm.onClick.AddListener(() => {
			Application.Quit();
		});
	}
}




