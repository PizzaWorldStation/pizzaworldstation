﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyHealth : MonoBehaviour
{
	public int startingHealth = 100;            // The amount of health the enemy starts the game with.
	public int currentHealth;                   // The current health the enemy has.
	public float sinkSpeed = 2.5f;              // The speed at which the enemy sinks through the floor when dead.
	public int scoreValue = 10;                 // The amount added to the player's score when the enemy dies.
	public AudioClip deathClip;                 // The sound to play when the enemy dies.
	public GameObject key;
	public float particleOffset;
	Animator anim;
	public GameObject deathParticles;
	//AudioSource enemyAudio;                     // Reference to the audio source.
	//ParticleSystem hitParticles;                // Reference to the particle system that plays when the enemy is damaged.
	bool isDead;                                // Whether the enemy is dead.
	bool isSinking;                             // Whether the enemy has started sinking through the floor.

    [FMODUnity.EventRef]
    private string FMOD_slimDeath = "event:/Enemies/HC_SlimeDeath";
    private string FMOD_goblinDeath1 = "event:/Enemies/HC_GoblinDeath1";
    private string FMOD_goblinDeath2 = "event:/Enemies/HC_GoblinDeath2";
    private string FMOD_goblinDeath3 = "event:/Enemies/HC_GoblinDeath3";
    private string FMOD_goblinDeath4 = "event:/Enemies/HC_GoblinDeath4";

    private List<string> FMOD_goblinDeathSounds;
    private int FMOD_randNum;

    void Awake ()
	{

        FMOD_goblinDeathSounds = new List<string>();
        FMOD_goblinDeathSounds.Add(FMOD_goblinDeath1);
        FMOD_goblinDeathSounds.Add(FMOD_goblinDeath2);
        FMOD_goblinDeathSounds.Add(FMOD_goblinDeath3);
        FMOD_goblinDeathSounds.Add(FMOD_goblinDeath4);

        // Setting up the references.
        anim = GetComponent <Animator> ();

		//enemyAudio = GetComponent <AudioSource> ();
		//hitParticles = GetComponentInChildren <ParticleSystem> ();

		// Setting the current health when the enemy first spawns.
		currentHealth = startingHealth;
	}


	void Update ()
	{
		/* If the enemy should be sinking...
		if(isSinking)
		{
			// ... move the enemy down by the sinkSpeed per second.
			transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
		}
		*/
	}



	public void TakeDamage (int amount, Vector3 hitPoint)
	{
		// If the enemy is dead...
		if(isDead)
			// ... no need to take damage so exit the function.
			return;
		
		// Play the hurt sound effect.
		//enemyAudio.Play ();

		// Reduce the current health by the amount of damage sustained.
		currentHealth -= amount;

		// Set the position of the particle system to where the hit was sustained.
		//hitParticles.transform.position = hitPoint;

		// And play the particles.
		//hitParticles.Play();

		// If the current health is less than or equal to zero...
		if(currentHealth <= 0)
		{
            if (this.gameObject.CompareTag("Slim")) {
                FMOD_playOnce(FMOD_slimDeath);
            }
            if (this.gameObject.CompareTag("Goblin")) {
                FMOD_makeGoblinDeath();
            }
            // ... the enemy is dead.
            Death ();
		}
	}


	void Death ()
	{
		if(this.gameObject.CompareTag("Boss")) {
			var keygen = Instantiate(key);
			keygen.transform.position = this.transform.position;
		}
		// The enemy is dead.
		isDead = true;
		//Instantiate (key, this.transform.position, this.transform.rotation);
		//anim.Play("Death");

		// Turn the collider into a trigger so shots can pass through it.
		/*
		capsuleCollider.isTrigger = true;

		// Tell the animator that the enemy is dead.
		anim.SetTrigger ("Dead");


		// Change the audio clip of the audio source to the death clip and play it (this will stop the hurt clip playing).
		enemyAudio.clip = deathClip;
		enemyAudio.Play ();
		*/

		//StartSinking ();
		var particles = (GameObject)Instantiate (deathParticles);
		float tempX = this.transform.position.x;
		float tempY = this.transform.position.y;
		float tempZ = this.transform.position.z;
		particles.transform.position = new Vector3(tempX,tempY+particleOffset, tempZ);
		Destroy (this.gameObject);
	}


	public void StartSinking ()
	{
		// Find and disable the Nav Mesh Agent.
		GetComponent <NavMeshAgent> ().enabled = false;

		// Find the rigidbody component and make it kinematic (since we use Translate to sink the enemy).
		GetComponent <Rigidbody> ().isKinematic = true;

		// The enemy should no sink.
		isSinking = true;

		// Increase the score by the enemy's score value.
		//ScoreManager.score += scoreValue;

		// After 2 seconds destory the enemy.
		Destroy (gameObject, 2f);
	}

    void FMOD_makeGoblinDeath() {
        FMOD_randNum = Random.Range(0, 4);
        FMODUnity.RuntimeManager.PlayOneShot(FMOD_goblinDeathSounds[FMOD_randNum]);
    }

    void FMOD_playOnce(string clip) {
        FMODUnity.RuntimeManager.PlayOneShot(clip);
    }
}