﻿using System.Linq;
using UnityEngine;
using System.Collections.Generic;
//using System;

public class ModularWorldGenerator : MonoBehaviour {
	public Module[] Modules;
	public Module StartModule;
    public GameObject[] items;

    // public GameObject slim;
    // public GameObject goblin;
    public GameObject[] basicEnemy;
    public GameObject[] bosses;

    public GameObject gunner;
    public GameObject mage;
    //public Module newModule;
    static public GameObject player;
    static public int playerChoice = 1;
	public int Iterations = 5;


    void Start() {

        if (playerChoice == 1) {
            player = gunner;
        } else if (playerChoice == 2) {
            player = mage;
        }

        var rooms = new List<Module>();
        var startModule = (Module)Instantiate(Modules[(Random.Range(0, Modules.Length))], transform.position, transform.rotation);//(Module)Instantiate(Modules[(Random.Range(0, Modules.Length))]);  //(Module)Instantiate(StartModule, transform.position, transform.rotation);
        var pendingExits = new List<ModuleConnector>(startModule.GetExits());
        var enemySpots = new List<EnemyConnector>();
        var playerSpawns = new List<PlayerConnector>(startModule.getPlayer());
        var playerSpawn = playerSpawns.FirstOrDefault();
        var itemSpots = new List<ItemConnector>();
        var newModuleExits = startModule.GetExits();
        var newModuleEnemies = startModule.GetEnemies();
        var newModuleItems = startModule.getItems();
        enemySpots.AddRange(newModuleEnemies);
        itemSpots.AddRange(newModuleItems);
        rooms.Add(startModule);


        for (int iteration = 0; iteration < Iterations; iteration++) {
            var newExits = new List<ModuleConnector>();
            var newEnemies = new List<EnemyConnector>();
            var newItems = new List<ItemConnector>();

            foreach (var pendingExit in pendingExits) {
                var newTag = GetRandom(pendingExit.Tags);
                var newModulePrefab = GetRandomWithTag(Modules, newTag);
                var newModule = Instantiate(newModulePrefab);
                //var newModuleExits = newModule.GetExits();
                //var newModuleEnemies = newModule.GetEnemies();
                //var newModuleItems = newModule.getItems();
                var exitToMatch = newModuleExits.FirstOrDefault(x => x.IsDefault) ?? GetRandom(newModuleExits);
                MatchExits(pendingExit, exitToMatch);

                foreach (var room in rooms) {
                    if (newModule.transform.GetChild(newModule.transform.childCount - 1).GetComponent<Renderer>().bounds.Intersects(room.transform.GetChild(room.transform.childCount - 1).GetComponent<Renderer>().bounds)) {
                        bool cd = true; //collisionDetected
                        print("collision Detected, replacing module");
                        DestroyObject(newModule);
                        var i = 0;
                        while (cd && i < Modules.Length)
                        {
                            newModule = Modules[i];
                            //newModuleExits = newModule.GetExits();
                            //exitToMatch = newModuleExits.FirstOrDefault(x => x.IsDefault) ?? GetRandom(newModuleExits);
                            //MatchExits(pendingExit, exitToMatch);
                            foreach (var room2 in rooms) {
                                if (!newModule.transform.GetChild(newModule.transform.childCount - 1).GetComponent<Renderer>().bounds.Intersects(room2.transform.GetChild(room2.transform.childCount - 1).GetComponent<Renderer>().bounds)) {
                                    cd = false;
                                    newModuleEnemies = newModule.GetEnemies();
                                    newModuleItems = newModule.getItems();
                                }
                            }
                            i++;
                        }
                    }
                }
                newExits.AddRange(newModuleExits.Where(e => e != exitToMatch));
                newEnemies.AddRange(newModuleEnemies);
                newItems.AddRange(newModuleItems);
                rooms.Add(newModule);
            }
            foreach (var enemy in newEnemies) {
                enemySpots.Add(enemy);
            }
            //			enemySpots = newEnemies;
            pendingExits = newExits;
            itemSpots = newItems;
        }

        //if (startModule.Tags.Contains("Boss")) {
        //    //	var newEnemies = new List<EnemyConnector>();
        //    var newModuleEnemies = startModule.GetEnemies();
        //    enemySpots.AddRange(newModuleEnemies);
        //}


        foreach (var remainingExit in pendingExits) {
            var curModule = (Module)Instantiate(Modules[Modules.Length - 1], transform.position, transform.rotation); // (Module)Instantiate(StartModule, transform.position, transform.rotation);
            //foreach (var room in rooms)
            //{
            //    if (curModule.transform.GetChild(curModule.transform.GetChildCount() - 1).GetComponent<Renderer>().bounds.Intersects(room.transform.GetChild(room.transform.GetChildCount() - 1).GetComponent<Renderer>().bounds))
            //    {
            //        curModule = (Module)Instantiate(Modules[Modules.Length - 1], transform.position, transform.rotation);
            //    }
            //}
            var curModuleExits = curModule.GetExits();
            var exitToMatch = curModuleExits.FirstOrDefault(x => x.IsDefault) ?? GetRandom(curModuleExits);
            MatchExits(remainingExit, exitToMatch);
        }

        foreach (var enemySpot in enemySpots) {
            //Random rnd = new Random();
            if (enemySpot.Tags.Contains("Slim"))
            {
                var currentEnemy = (GameObject)Instantiate(basicEnemy[(Random.Range(0, basicEnemy.Length) % basicEnemy.Length)]);
                currentEnemy.transform.position = enemySpot.transform.position;
            }
            else if (enemySpot.Tags.Contains("Goblin"))
            {
                var currentEnemy = (GameObject)Instantiate(basicEnemy[(Random.Range(0, basicEnemy.Length) % basicEnemy.Length)]);
                currentEnemy.transform.position = enemySpot.transform.position;
            }
            else if (enemySpot.Tags.Contains("Boss"))
            {
                var currentEnemy = (GameObject)Instantiate(bosses[(Random.Range(0, bosses.Length) % bosses.Length)]);
                currentEnemy.transform.position = enemySpot.transform.position;
            }
        }
        print("about to start items");
        foreach (var itemSpot in itemSpots)
        {
            print("placing item");
            var curItem = GetRandom(items);
            var newItem = Instantiate(curItem);
            newItem.transform.position = itemSpot.transform.position;
        }

        //var bossSpot = enemySpots[0];
        //var bossEnemy = (GameObject)Instantiate(bosses[(Random.Range(0, bosses.Length) % bosses.Length)]);
        //bossEnemy.transform.position = bossSpot.transform.position;




        //if (itemSpots.Count > 0)
        //{
        //    var keySpot = itemSpots[0];
        //    var keything = (GameObject)Instantiate(items[0]);
        //    keything.transform.position = keySpot.transform.position;
        //}

        player.transform.position = playerSpawn.transform.position; 
//		int roomCount = 0;
//		foreach (var room in rooms) {
//			print("room: " + roomCount + "\n");
//			roomCount++;
//		}
    }

	private void MatchExits(ModuleConnector oldExit, ModuleConnector newExit) {
		var newModule = newExit.transform.parent;
		var forwardVectorToMatch = -oldExit.transform.forward;
		var correctiveRotation = Azimuth(forwardVectorToMatch) - Azimuth(newExit.transform.forward);
		newModule.RotateAround(newExit.transform.position, Vector3.up, correctiveRotation);
		var correctiveTranslation = oldExit.transform.position - newExit.transform.position;
		newModule.transform.position += correctiveTranslation;
	}


	private static TItem GetRandom<TItem>(TItem[] array) {
        Random.seed = System.DateTime.Now.Millisecond;
        int shit = Random.Range(0, array.Length-1);
        print(shit);
        return array[shit];
	}


	private static Module GetRandomWithTag(IEnumerable<Module> modules, string tagToMatch) {
		var matchingModules = modules.Where(m => m.Tags.Contains(tagToMatch)).ToArray();
		return GetRandom(matchingModules);
	}


	private static float Azimuth(Vector3 vector) {
		return Vector3.Angle(Vector3.forward, vector) * Mathf.Sign(vector.x);
	}
}