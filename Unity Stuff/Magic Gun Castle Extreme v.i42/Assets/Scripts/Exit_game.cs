﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIPauseButton : MonoBehaviour {

	public Button exit;
	public Button confirm;
	public Button cancel;
	public GameObject g;

	void start ()
	{	}

	void Update () 
	{
		exit.onClick.AddListener(()=> {
		g.SetActive(true);
		});

		cancel.onClick.AddListener(()=> {
		g.SetActive(false);
		});
		
		confirm.onClick.AddListener(() => {
		Application.Quit();
		});
	}

}
