﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Timers;

public class StartScreenBackground : MonoBehaviour {

	private Color color;
	private Color defaultC;
	private float timeStamp;
	private float timeTriggered;
	private bool secondT;

    [FMODUnity.EventRef]
    private string FMOD_thunder1 = "event:/Menu/HC_Thunder1";
    private string FMOD_thunder2 = "event:/Menu/HC_Thunder2";
    private string FMOD_thunder3 = "event:/Menu/HC_Thunder3";
    private string FMOD_thunder4 = "event:/Menu/HC_Thunder4";

    private List<string> FMOD_thunderSounds;
    private int FMOD_randNum;


    void Start () 
	{
		color = GetComponent<Image> ().color;
		defaultC = color;

        timeStamp = Time.time + 2;
		secondT = false;

        FMOD_thunderSounds = new List<string>();
        FMOD_thunderSounds.Add(FMOD_thunder1);
        FMOD_thunderSounds.Add(FMOD_thunder2);
        FMOD_thunderSounds.Add(FMOD_thunder3);
        FMOD_thunderSounds.Add(FMOD_thunder4);

    }
	
	// Update is called once per frame
	void Update () 
	{
        //color = new Color (0, 0, 0);
        //Random.seed = 10;

        if (timeStamp < Time.time) 
		{
			color = new Color(255,255,255,255);
			GetComponent<Image> ().color = color;
			timeTriggered = Time.time + 1;
            timeStamp = Time.time + Random.value + 7;
            

			secondStrike ();

            //Put trigger here for Thunder sound
            FMOD_makeThunder();
        } 
		else 
		{
			color = defaultC;
			GetComponent<Image> ().color = color;
		}

		if ((timeTriggered < Time.time) && secondT)
		{
			color = new Color(255,255,255,255);
			GetComponent<Image> ().color = color;

			secondT = false;

            //Put trigger here for Thunder sound
            FMOD_makeThunder();
        } 
		/*else 
		{
			color = defaultC;
			GetComponent<Image> ().color = color;
		}*/
	}

	private void secondStrike()
	{
		if (Random.value > 5) 
		{
			secondT = true;
		}
	}

    void FMOD_makeThunder() {
       FMOD_randNum = Random.Range(0, 4);
       FMODUnity.RuntimeManager.PlayOneShot(FMOD_thunderSounds[FMOD_randNum]);
    }
}
