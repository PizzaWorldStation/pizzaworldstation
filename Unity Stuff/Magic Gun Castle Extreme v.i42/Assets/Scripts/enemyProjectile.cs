﻿using UnityEngine;
using System.Collections;

public class enemyProjectile : MonoBehaviour {

	public Vector3 moveTowards;
	Transform player;
	public float speed = 5f;
	public float rotationSpeed = 10f;
	public int projDamage = 10;


	
	// Update is called once per frame
	void Update () {
		transform.position += transform.forward * speed * Time.deltaTime;
	}
	void OnTriggerEnter(Collider other)
	{
		
		if (other.tag == "Player") {
			Destroy (this.gameObject);
			other.SendMessage ("ApplyDamage", projDamage);
		} else if (other.tag == "Wall") {
			Destroy (this.gameObject);
		}
	}
}
