﻿using UnityEngine;
using System.Collections;

public class CorridorCollision : MonoBehaviour {
    EnemyController parentController;
    Animator parentAnim;

    void Start() {
        parentController = this.transform.parent.GetComponent<EnemyController>();
        parentAnim = this.transform.parent.GetComponent<Animator>();
    }

    void OnCollisionEnter(Collision other) {

        if (other.gameObject.tag == "Corridor") {
                print("Corridor collllllllision");
            parentController.playerInRange = true;
            parentAnim.SetBool("isWalking", true);
            Destroy(other.gameObject);
            }
    }
}