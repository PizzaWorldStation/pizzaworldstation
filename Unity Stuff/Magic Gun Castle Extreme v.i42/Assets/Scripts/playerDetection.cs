﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class playerDetection : MonoBehaviour
{

	EnemyController parentController; 
	Animator parentAnim;

    [FMODUnity.EventRef]
    private string FMOD_goblin1 = "event:/Enemies/HC_Goblin1";
    private string FMOD_goblin2 = "event:/Enemies/HC_Goblin2";
    private string FMOD_goblin3 = "event:/Enemies/HC_Goblin3";
    private string FMOD_goblin4 = "event:/Enemies/HC_Goblin4";
    private string FMOD_goblin5 = "event:/Enemies/HC_Goblin5";

    private List<string> FMOD_goblinSounds;
    private int FMOD_randNum;
    private bool FMOD_playing;

    void Start()
	{
		parentController = this.transform.parent.GetComponent<EnemyController>();
		parentAnim = this.transform.parent.GetComponent<Animator>();


        FMOD_playing = false;
        FMOD_goblinSounds = new List<string>();
        FMOD_goblinSounds.Add(FMOD_goblin1);
        FMOD_goblinSounds.Add(FMOD_goblin2);
        FMOD_goblinSounds.Add(FMOD_goblin3);
        FMOD_goblinSounds.Add(FMOD_goblin4);
        FMOD_goblinSounds.Add(FMOD_goblin5);
    }

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") {
            parentController.playerInRange = true;
			parentAnim.SetBool ("isWalking", true);

            if (this.gameObject.name == "Jason's Goblin(Clone)") {
                FMOD_makeGoblinSound();
            }
            

        }
	}

    void FMOD_makeGoblinSound() {
        FMOD_randNum = Random.Range(0, 5);
        FMODUnity.RuntimeManager.PlayOneShot(FMOD_goblinSounds[FMOD_randNum]);
    }
}