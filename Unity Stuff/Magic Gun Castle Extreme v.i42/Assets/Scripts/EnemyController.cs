﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyController : MonoBehaviour
{
	Transform player;               // Reference to the player's position.
	GameObject playerObj;


	//NavMeshAgent nav; 
	public float speed = 5f;
	public float defaultSpeed = 5f;
	public float rotationSpeed = 3f; //speed of turning
	public bool playerInRange = false;
	public bool attacking = false;
	Animator anim;
	Animation curAnim;
	Vector3 playerPos;
	Vector3 thisPos;
	Vector3 dist;
	//Enemy Type
	public bool isBoss;
	public bool isRanged;

	//Attack Vars
	public float attackDist = 0.5f;
	public float attackRate = 10f;
	public float attackCoolDown;
	public int damage;

	//Summon Vars
	public float summonRate = 10f;
	public float summonCoolDown = 0f;
	public GameObject slim;
	public GameObject projectile;
	public int maxSpawns = 3;
	int numSpawns = 0;
	Vector3[] positionArray = new Vector3[11];

	//Stagger Vars
	public float staggerCoolDown;
	public float staggerTime = 0.25f;
	public float staggeredSpeed;
	bool staggered;


    [FMODUnity.EventRef]
    private string FMOD_suspenseBg = "event:/Level/HC_Suspense";


    [FMODUnity.EventRef]
    private string FMOD_slimeShoot = "event:/Enemies/HC_SlimeShoot";
    private string FMOD_mageShoot = "event:/Enemies/HC_ElectricShoot";
    private string FMOD_bossShoot = "event:/Enemies/HC_FireSpell";

    [FMODUnity.EventRef]
    private string FMOD_goblin1 = "event:/Enemies/HC_Goblin1";
    private string FMOD_goblin2 = "event:/Enemies/HC_Goblin2";
    private string FMOD_goblin3 = "event:/Enemies/HC_Goblin3";
    private string FMOD_goblin4 = "event:/Enemies/HC_Goblin4";
    private string FMOD_goblin5 = "event:/Enemies/HC_Goblin5";

    [FMODUnity.EventRef]
    private string FMOD_BossOne1 = "event:/Enemies/HC_BossOne1";
    private string FMOD_BossOne2 = "event:/Enemies/HC_BossOne2";
    private string FMOD_BossOne3 = "event:/Enemies/HC_BossOne3";
    private string FMOD_BossOne4 = "event:/Enemies/HC_BossOne4";
    private string FMOD_BossOne5 = "event:/Enemies/HC_BossOne5";

    private List<string> FMOD_goblinSounds;
    private List<string> FMOD_bossSounds;
    private int FMOD_randNum;
    private bool FMOD_isPlaying;
    //private bool FMOD_isPlayingBossOne;

    void Start ()
	{
		// Set up the references.
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		playerObj = GameObject.FindGameObjectWithTag ("Player");

		anim = GetComponent<Animator>();
		attackCoolDown = 0;
		staggerCoolDown = 0;
		staggered = false;
		speed = defaultSpeed;
		if (isBoss == true)
		{
//			positionArray [0] = new Vector3 (9.0f, 0.0f, 6.0f);            
//			positionArray [1] = new Vector3 (1f, 0f, 1f);
//			positionArray [2] = new Vector3 (14.0f, 0f, 0.4f);
//			positionArray [3] = new Vector3 (-3.0f, 0f, -13.0f);
//			positionArray [4] = new Vector3 (9.0f, 0.0f, 6.0f);            
//			positionArray [5] = new Vector3 (1f, 0f, 10f);
//			positionArray [6] = new Vector3 (5.0f, 0f, -18f);
//			positionArray [7] = new Vector3 (-3.0f, 0f, -13.0f);
//			positionArray [8] = new Vector3 (9.0f, 0.0f, -6.0f);            
//			positionArray [8] = new Vector3 (15f, 0f, -16f);
//			positionArray [10] = new Vector3 (-14.0f, 0f, 5f);
			summonCoolDown = summonRate;

           
        }
        FMOD_isPlaying = false;
        //FMOD_isPlayingBossOne = false;
        FMOD_goblinSounds = new List<string>();
        FMOD_goblinSounds.Add(FMOD_goblin1);
        FMOD_goblinSounds.Add(FMOD_goblin2);
        FMOD_goblinSounds.Add(FMOD_goblin3);
        FMOD_goblinSounds.Add(FMOD_goblin4);
        FMOD_goblinSounds.Add(FMOD_goblin5);

        FMOD_bossSounds = new List<string>();
        FMOD_bossSounds.Add(FMOD_BossOne1);
        FMOD_bossSounds.Add(FMOD_BossOne2);
        FMOD_bossSounds.Add(FMOD_BossOne3);
        FMOD_bossSounds.Add(FMOD_BossOne4);
        FMOD_bossSounds.Add(FMOD_BossOne5);



    }
	void OnCollisionEnter(Collision collision) 
	{
		if (collision.gameObject.tag == "Player") 
		{
			//anim.Play ("Attacking");
			attacking = true;
			//attackWait ();

		}
	}

	//Update Function
	void Update ()
	{
		
		if (isRanged == false) {
			meleeAttackMove ();
		}
		else if (isRanged == true)
		{
			rangedAttackMove ();
		}

        

		checkStagger ();
		checkSummon ();



	} 
	//Attack and Movement functions
	void meleeAttackMove()
	{
		if (playerInRange == true && (Vector3.Distance(player.position, transform.position) > attackDist))
		{
            if (this.tag == "Goblin" && FMOD_isPlaying == false) {
                    FMOD_makeGoblinSound();
                FMOD_isPlaying = true;
            }

            if (this.tag == "Boss" && FMOD_isPlaying == false) {
                FMOD_makeBossSound();
                
                FMOD_isPlaying = true;
            }
			transform.rotation = Quaternion.Slerp (transform.rotation,Quaternion.LookRotation (player.position - transform.position), rotationSpeed * Time.deltaTime);
			transform.position += transform.forward * speed * Time.deltaTime;
			attacking = false;
			anim.SetBool ("isWalking", true);
		} 
		else if(Vector3.Distance(player.position, transform.position) < attackDist)
		{
			if (attackCoolDown > 0) {
				attackCoolDown -= Time.deltaTime;
			}
			else 
			{
				Attack ();
				attackCoolDown = attackRate;
			}

			transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (player.position - transform.position), rotationSpeed * Time.deltaTime);
		}
	}
	void rangedAttackMove()
	{
		if (playerInRange == true && (Vector3.Distance(player.position, transform.position) > attackDist))
		{
			transform.rotation = Quaternion.Slerp (transform.rotation,Quaternion.LookRotation (player.position - transform.position), rotationSpeed * Time.deltaTime);
			transform.position += transform.forward * speed * Time.deltaTime;
			attacking = false;
			anim.SetBool ("isWalking", true);


        } 
		else if(Vector3.Distance(player.position, transform.position) < attackDist)
		{
			anim.SetBool ("isWalking", false);
			if (attackCoolDown > 0) {
				attackCoolDown -= Time.deltaTime;
			}
			else 
			{
				AttackRanged ();
				attackCoolDown = attackRate;

                if (this.tag == "Enemy") {
                    FMODUnity.RuntimeManager.PlayOneShot(FMOD_mageShoot);
                }
                if (this.name == "Boss(Goblin)(Clone)") {
                    FMODUnity.RuntimeManager.PlayOneShot(FMOD_bossShoot);
                }
            }

			transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.LookRotation (player.position - transform.position), rotationSpeed * Time.deltaTime);
		}
	}
	void Attack()
	{
		playerObj.SendMessage ("ApplyDamage", damage);
		anim.Play ("Attacking");
	}
	void AttackRanged()
	{
		
		anim.Play ("Attacking");
		var proj = (GameObject)Instantiate (projectile);
		float tempX = this.transform.position.x;
		float tempY = this.transform.position.y;
		float tempZ = this.transform.position.z;
		proj.transform.position = new Vector3(tempX,tempY+1f, tempZ);
		proj.transform.rotation = Quaternion.Slerp (transform.rotation,Quaternion.LookRotation (player.position - transform.position), rotationSpeed * Time.deltaTime);

        if (this.tag == "Slim") {
            FMODUnity.RuntimeManager.PlayOneShot(FMOD_slimeShoot);
        }
	

	}
	//Check summon cooldown etc
	void checkSummon()
	{
		summonCoolDown -= Time.deltaTime;
		if (isBoss == true && playerInRange == true)
		{
			
			if (summonCoolDown <= 0)
			{
				Summon ();
				summonCoolDown = summonRate;
			}
		}
	}
	//Check stagger cooldowns etc
	void checkStagger()
	{
		if (staggerCoolDown > 0) {
			staggerCoolDown -= Time.deltaTime;
		} 
		else if (staggerCoolDown <= 0)
		{
			speed = defaultSpeed;
		}
	}
	//Slow on impact
	public void Stagger()
	{
		//If not currently staggered, stagger
		if (staggerCoolDown <= 0) {
			speed = staggeredSpeed;
			staggerCoolDown = staggerTime;
		} 

	}
	//Summon minions
	void Summon()
	{
		
		//while (numSpawns < maxSpawns)
		for(int i = 0; i < maxSpawns; i ++)
		{
			var spawn = (GameObject)Instantiate (slim);
			float tempX = this.transform.position.x;
			float tempZ = this.transform.position.z;
			//spawn.transform.position = positionArray[Random.Range(0,10)];

			spawn.transform.position = new Vector3 (tempX + Random.Range (0, i+1), 0, tempZ + Random.Range (0, i+1));
				numSpawns++;



		}
		numSpawns = 0;

	}

    void FMOD_makeGoblinSound() {
        FMOD_randNum = Random.Range(0, 5);
        FMODUnity.RuntimeManager.PlayOneShot(FMOD_goblinSounds[FMOD_randNum]);
    }
    void FMOD_makeBossSound() {
        FMOD_randNum = Random.Range(0, 5);
        FMODUnity.RuntimeManager.PlayOneShot(FMOD_bossSounds[FMOD_randNum]);
        FMODUnity.RuntimeManager.PlayOneShot(FMOD_suspenseBg);
    }

}